namespace GuavaCheckIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubmissionStatus1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Submissions", "SubmissionStatusCode", c => c.String(maxLength: 128));
            CreateIndex("dbo.Submissions", "SubmissionStatusCode");
            AddForeignKey("dbo.Submissions", "SubmissionStatusCode", "dbo.SubmissionStatus", "Code");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Submissions", "SubmissionStatusCode", "dbo.SubmissionStatus");
            DropIndex("dbo.Submissions", new[] { "SubmissionStatusCode" });
            DropColumn("dbo.Submissions", "SubmissionStatusCode");
        }
    }
}
