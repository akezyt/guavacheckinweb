namespace GuavaCheckIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdddAddressToSubmission : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Submissions", "Address", c => c.String());
            AddColumn("dbo.Submissions", "Road", c => c.String());
            AddColumn("dbo.Submissions", "Tambol", c => c.String());
            AddColumn("dbo.Submissions", "Amphur", c => c.String());
            AddColumn("dbo.Submissions", "Province", c => c.String());
            AddColumn("dbo.Submissions", "Phone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Submissions", "Phone");
            DropColumn("dbo.Submissions", "Province");
            DropColumn("dbo.Submissions", "Amphur");
            DropColumn("dbo.Submissions", "Tambol");
            DropColumn("dbo.Submissions", "Road");
            DropColumn("dbo.Submissions", "Address");
        }
    }
}
