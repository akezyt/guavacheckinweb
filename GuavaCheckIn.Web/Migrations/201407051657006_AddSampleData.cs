namespace GuavaCheckIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSampleData : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO Submissions(Date, EnteredDate, EnteredBy, ArrivalCardNumber, SubmissionStatusCode, Address, Road, Tambol, Amphur, Province, Phone) 
                VALUES('2014-7-5', '2014-7-5' ,'EnteredBy1' ,'ArrivalCardNumber1' ,'SU' ,'Address1' ,'Road1' ,'Tambol1' ,'Amphur1' ,'Province1' ,'Phone1')");

            Sql(@"INSERT INTO Submissions(Date, EnteredDate, EnteredBy, ArrivalCardNumber, SubmissionStatusCode, Address, Road, Tambol, Amphur, Province, Phone) 
                VALUES('2014-7-5', '2014-7-5' ,'EnteredBy2' ,'ArrivalCardNumber2' ,'CO' ,'Address2' ,'Road2' ,'Tambol2' ,'Amphur2' ,'Province2' ,'Phone2')");

            Sql(@"INSERT INTO Submissions(Date, EnteredDate, EnteredBy, ArrivalCardNumber, SubmissionStatusCode, Address, Road, Tambol, Amphur, Province, Phone) 
                VALUES('2014-7-5', '2014-7-5' ,'EnteredBy3' ,'ArrivalCardNumber3' ,'CO' ,'Address3' ,'Road3' ,'Tambol3' ,'Amphur3' ,'Province3' ,'Phone3')");

            Sql(@"INSERT INTO Blobs(Url) 
                VALUES('Documents/1.jpg')");

            Sql(@"INSERT INTO Blobs(Url) 
                VALUES('Documents/2.jpg')");

            Sql(@"INSERT INTO Blobs(Url) 
                VALUES('Documents/3.png')");

            Sql(@"INSERT INTO Blobs(Url) 
                VALUES('Documents/4.png')");

            Sql(@"INSERT INTO Blobs(Url) 
                VALUES('Documents/5.jpg')");

            Sql(@"INSERT INTO Blobs(Url) 
                VALUES('Documents/6.jpg')");

            Sql(@"INSERT INTO SubmissionDocuments(SubmissionId, SubmissionDocumentTypeCode, BlobId, CreatedDate) 
                VALUES('1', 'ES', '1', '2014-7-6')");

            Sql(@"INSERT INTO SubmissionDocuments(SubmissionId, SubmissionDocumentTypeCode, BlobId, CreatedDate) 
                VALUES('1', 'VS', '2', '2014-7-6')");

            Sql(@"INSERT INTO SubmissionDocuments(SubmissionId, SubmissionDocumentTypeCode, BlobId, CreatedDate) 
                VALUES('1', 'DC', '3', '2014-7-6')");

            Sql(@"INSERT INTO SubmissionDocuments(SubmissionId, SubmissionDocumentTypeCode, BlobId, CreatedDate) 
                VALUES('1', 'PN', '4', '2014-7-6')");

            Sql(@"INSERT INTO SubmissionDocuments(SubmissionId, SubmissionDocumentTypeCode, BlobId, CreatedDate) 
                VALUES('1', 'VE', '5', '2014-7-6')");

            Sql(@"INSERT INTO SubmissionDocuments(SubmissionId, SubmissionDocumentTypeCode, BlobId, CreatedDate) 
                VALUES('1', 'PP', '6', '2014-7-6')");

            Sql(@"UPDATE SubmissionDocumentTypes
                SET [Order]='1'
                WHERE Code='ES'");

            Sql(@"UPDATE SubmissionDocumentTypes
                SET [Order]='2'
                WHERE Code='VS'");

            Sql(@"UPDATE SubmissionDocumentTypes
                SET [Order]='3'
                WHERE Code='DC'");

            Sql(@"UPDATE SubmissionDocumentTypes
                SET [Order]='4'
                WHERE Code='PN'");

            Sql(@"UPDATE SubmissionDocumentTypes
                SET [Order]='5'
                WHERE Code='VE'");

            Sql(@"UPDATE SubmissionDocumentTypes
                SET [Order]='6'
                WHERE Code='PP'");

        }
        
        public override void Down()
        {
        }
    }
}
