namespace GuavaCheckIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubmissionDocumentType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubmissionDocumentTypes",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Code);

            Sql("INSERT INTO SubmissionDocumentTypes(Code, Name, [Order]) VALUES('PP', 'Passport', 1)");
            Sql("INSERT INTO SubmissionDocumentTypes(Code, Name, [Order]) VALUES('VS', 'Visa', 2)");
            Sql("INSERT INTO SubmissionDocumentTypes(Code, Name, [Order]) VALUES('ES', 'Entry Stamp', 3)");
            Sql("INSERT INTO SubmissionDocumentTypes(Code, Name, [Order]) VALUES('VE', 'Visa Extension', 4)");
            Sql("INSERT INTO SubmissionDocumentTypes(Code, Name, [Order]) VALUES('DC', 'Departure Card', 5)");
            Sql("INSERT INTO SubmissionDocumentTypes(Code, Name, [Order]) VALUES('PN', 'Previous Notification', 6)");
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SubmissionDocumentTypes");
        }
    }
}
