namespace GuavaCheckIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateSubmissionStatus : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO SubmissionStatus(Code, Name) VALUES('CR', 'Created')");
            Sql("INSERT INTO SubmissionStatus(Code, Name) VALUES('SU', 'Submitted')");
            Sql("INSERT INTO SubmissionStatus(Code, Name) VALUES('MA', 'Mailed')");
            Sql("INSERT INTO SubmissionStatus(Code, Name) VALUES('CO', 'Completed')");
        }
        
        public override void Down()
        {
        }
    }
}
