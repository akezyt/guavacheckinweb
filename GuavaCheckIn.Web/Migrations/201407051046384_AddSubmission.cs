namespace GuavaCheckIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubmission : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Submissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        EnteredDate = c.DateTime(nullable: false),
                        EnteredBy = c.String(),
                        ArrivalCardNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Submissions");
        }
    }
}
