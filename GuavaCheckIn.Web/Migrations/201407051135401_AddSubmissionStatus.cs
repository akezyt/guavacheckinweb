namespace GuavaCheckIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubmissionStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubmissionStatus",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
            AddColumn("dbo.SubmissionDocuments", "SubmissionStatusCode", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.SubmissionDocuments", "SubmissionStatusCode");
            AddForeignKey("dbo.SubmissionDocuments", "SubmissionStatusCode", "dbo.SubmissionStatus", "Code", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubmissionDocuments", "SubmissionStatusCode", "dbo.SubmissionStatus");
            DropIndex("dbo.SubmissionDocuments", new[] { "SubmissionStatusCode" });
            DropColumn("dbo.SubmissionDocuments", "SubmissionStatusCode");
            DropTable("dbo.SubmissionStatus");
        }
    }
}
