namespace GuavaCheckIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubmissionDocuments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubmissionDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubmissionId = c.Int(nullable: false),
                        SubmissionDocumentTypeCode = c.String(nullable: false, maxLength: 128),
                        BlobId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Blobs", t => t.BlobId, cascadeDelete: true)
                .ForeignKey("dbo.SubmissionDocumentTypes", t => t.SubmissionDocumentTypeCode, cascadeDelete: true)
                .ForeignKey("dbo.Submissions", t => t.SubmissionId, cascadeDelete: true)
                .Index(t => t.SubmissionId)
                .Index(t => t.SubmissionDocumentTypeCode)
                .Index(t => t.BlobId);
            
            CreateTable(
                "dbo.Blobs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubmissionDocuments", "SubmissionId", "dbo.Submissions");
            DropForeignKey("dbo.SubmissionDocuments", "SubmissionDocumentTypeCode", "dbo.SubmissionDocumentTypes");
            DropForeignKey("dbo.SubmissionDocuments", "BlobId", "dbo.Blobs");
            DropIndex("dbo.SubmissionDocuments", new[] { "BlobId" });
            DropIndex("dbo.SubmissionDocuments", new[] { "SubmissionDocumentTypeCode" });
            DropIndex("dbo.SubmissionDocuments", new[] { "SubmissionId" });
            DropTable("dbo.Blobs");
            DropTable("dbo.SubmissionDocuments");
        }
    }
}
