namespace GuavaCheckIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveStatusFromSubmissionDocument : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SubmissionDocuments", "SubmissionStatusCode", "dbo.SubmissionStatus");
            DropIndex("dbo.SubmissionDocuments", new[] { "SubmissionStatusCode" });
            DropColumn("dbo.SubmissionDocuments", "SubmissionStatusCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubmissionDocuments", "SubmissionStatusCode", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.SubmissionDocuments", "SubmissionStatusCode");
            AddForeignKey("dbo.SubmissionDocuments", "SubmissionStatusCode", "dbo.SubmissionStatus", "Code", cascadeDelete: true);
        }
    }
}
