
var blob = null;
var base64 = null;
var uploadType = "";
var next = "";

$('input[type=file]').on('change', function () {
    var file = $('input[type=file]');
    var reader = new FileReader();
    reader.readAsDataURL(file.get(0).files[0]);

    reader.onload = function (event) {
        base64 = reader.result;

        //$('#base64').attr('value', base64);
        blob = dataURLToBlob(base64);

        $('.img-guide').attr('src', base64);
    }

    return false;
})
$('form').on('submit', function (event) {

    //var formData = new FormData();
    //formData.append('step1', blob);
    //window.location = '/ui/checkin/' + next;

    $.ajax({
        url: '/api/submissiondocument',
        type: 'POST',
        data: {
            "Content": base64,
            "Type": uploadType
        },
        cache: false
    }).done(function () {
        window.location = '/ui/checkin/' + next;
    }).fail(function (x, status, error) {
        alert("Something wrong: " + error);
    });

    return false;
});