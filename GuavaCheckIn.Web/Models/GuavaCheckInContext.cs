﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GuavaCheckIn.Web.Models
{
    public class GuavaCheckInContext : DbContext
    {
        public GuavaCheckInContext()
            : base("DefaultConnection")
        {
            
        }

        public DbSet<Submission> Submissions { get; set; }

        public DbSet<SubmissionDocumentType> SubmissionDocumentTypes { get; set; }

        public DbSet<SubmissionDocument> SubmissionDocuments { get; set; }

        public DbSet<SubmissionStatus> SubmissionStatuses { get; set; }

        public DbSet<Blob> Blobs { get; set; }

        
    }
}