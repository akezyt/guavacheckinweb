﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuavaCheckIn.Web.Models
{
    public class SubmissionDocument
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int SubmissionId { get; set; }

        [Required]
        public string SubmissionDocumentTypeCode { get; set; }

        public virtual SubmissionDocumentType SubmissionDocumentType { get; set; }

        [Required]
        public int BlobId { get; set; }

        public virtual Blob Blob { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}