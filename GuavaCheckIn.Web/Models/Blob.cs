﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuavaCheckIn.Web.Models
{
    public class Blob
    {
        [Key]
        public int Id { get; set; }

        public string Url { get; set; }
    }
}