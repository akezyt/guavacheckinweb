﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuavaCheckIn.Web.Models
{
    public class SubmissionDocumentType
    {
        [Key]
        public string Code { get; set; }

        public string Name { get; set; }

        [Required]
        public int Order { get; set; }
    }
}