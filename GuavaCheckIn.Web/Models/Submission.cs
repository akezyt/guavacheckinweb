﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuavaCheckIn.Web.Models
{
    public class Submission
    {
        [Key]
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public DateTime EnteredDate { get; set; }

        public string EnteredBy { get; set; }

        public string ArrivalCardNumber { get; set; }

        public virtual List<SubmissionDocument> SubmissionDocuments { get; set; }

        public string SubmissionStatusCode {get;set;}

        public virtual SubmissionStatus SubmissionStatus { get; set; }

        public string Address { get; set; }

        public string Road { get; set; }

        public string Tambol { get; set; }

        public string Amphur { get; set; }

        public string Province { get; set; }

        public string Phone { get; set; }
    }
}