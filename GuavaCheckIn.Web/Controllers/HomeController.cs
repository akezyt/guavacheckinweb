﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GuavaCheckIn.Web.Models;
using System.Net;

namespace GuavaCheckIn.Web.Controllers
{
    public class HomeController : Controller
    {
        private GuavaCheckInContext guavaCheckInContext;

        public HomeController()
        {
            guavaCheckInContext = new GuavaCheckIn.Web.Models.GuavaCheckInContext();
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Manager()
        {
            ViewBag.Title = "Manager";

            List<Submission> submissionList = guavaCheckInContext.Submissions.OrderBy(s => s.Id).ToList();

            return View(submissionList);
        }

        public ActionResult Documents(int id)
        {
            //string currentPath = ControllerContext.HttpContext.Server.MapPath("~");

            ViewBag.Title = "Documents";
            ViewBag.SubmissionId = id;
            //ViewBag.CurrentPath = currentPath;

            List<SubmissionDocument> submissionDocumentList = guavaCheckInContext.SubmissionDocuments.Where(s => s.SubmissionId == id).OrderBy(s => s.SubmissionDocumentType.Order).ToList();

            return View(submissionDocumentList);
        }

        public ActionResult Print(int id)
        {
            ViewBag.Title = "Print";

            //byte[] bytes = System.IO.File.ReadAllBytes("guavacheckin.azurewebsites.net/Content/Documents.pdf");
            WebClient myClient = new WebClient();
            byte[] bytes = myClient.DownloadData("http://guavacheckin.azurewebsites.net/Content/Documents.pdf");
            return File(bytes, "application/pdf", "Documents.pdf");
        }
    }
}
