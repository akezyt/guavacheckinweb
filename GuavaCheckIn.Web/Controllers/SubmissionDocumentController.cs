﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using GuavaCheckIn.Web.Models;
using Microsoft.WindowsAzure.Storage;

namespace GuavaCheckIn.Web.Controllers
{
    public class SubmissionDocumentAPIModel
    {
        public static int SubmissionId { get; set; }

        public string Type { get; set; }

        public string Content { get; set; }
    }

    public class SubmissionDocumentController : ApiController
    {
        [HttpPost]
        public SubmissionDocument Post(SubmissionDocumentAPIModel value)
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);

            var blobClient = storageAccount.CreateCloudBlobClient();

            var container = blobClient.GetContainerReference("submissiondocument");

            var commaIndex = value.Content.IndexOf(",");

            var actualContext = value.Content.Substring(commaIndex + 1, value.Content.Length - commaIndex - 1);

            var bytes = Convert.FromBase64String(actualContext);

            using (var ctx = new GuavaCheckInContext())
            {
                var blockBlob = container.GetBlockBlobReference(SubmissionDocumentAPIModel.SubmissionId.ToString() + "/" + value.Type + "-" + DateTime.Now.ToString("yyyyddMMhhmmss") + ".jpeg");

                blockBlob.UploadFromByteArray(bytes, 0, bytes.Length);

                var blob = new Blob() { Url = blockBlob.Uri.ToString() };
                ctx.Blobs.Add(blob);

                var doc = new SubmissionDocument();
                if (SubmissionDocumentAPIModel.SubmissionId != 0)
                {
                    doc.SubmissionId = SubmissionDocumentAPIModel.SubmissionId;
                }else
                {
                    doc.SubmissionId = ctx.Submissions.Max(s => s.Id);
                }

                doc.SubmissionDocumentTypeCode = value.Type;
                doc.BlobId = blob.Id;
                doc.CreatedDate = DateTime.Now;

                ctx.SubmissionDocuments.Add(doc);

                ctx.SaveChanges();

                return doc;
            }
        }
    }
}
