﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GuavaCheckIn.Web.Models;

namespace GuavaCheckIn.Web.Controllers
{
    public class SubmissionController : ApiController
    {
        // PUT api/submission/5
        public Submission Put(Submission submission)
        {
            if (ModelState.IsValid)
            {
                submission.SubmissionStatusCode = "CR";

                submission.SubmissionDocuments = null;

                using (var ctx = new GuavaCheckInContext())
                {
                    ctx.Submissions.Add(submission);

                    ctx.SaveChanges();

                    SubmissionDocumentAPIModel.SubmissionId = submission.Id;

                    return submission;
                }
            }

            throw new InvalidOperationException();
        }
    }
}
