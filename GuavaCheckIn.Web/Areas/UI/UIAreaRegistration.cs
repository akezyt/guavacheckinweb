﻿using System.Web.Mvc;

namespace GuavaCheckIn.Web.Areas.UI
{
    public class UIAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "UI";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "UI_default",
                "ui/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
