﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuavaCheckIn.Web.Areas.UI.Controllers
{
    public class CheckInController : Controller
    {
        //
        // GET: /CheckIn/Instruction

        public ActionResult Instruction()
        {
            return View();
        }

        public ActionResult Profile()
        {
            return View();
        }

        [ActionName("Profile")]
        [HttpPost]
        public ActionResult PostProfile()
        {
            return RedirectToAction("Step1", new { area = "UI" });
        }

        //public ActionResult ProfilePhoto()
        //{
        //    return View();
        //}

        //[ActionName("ProfilePhoto")]
        //[HttpPost]
        //public ActionResult PostProfilePhoto()
        //{
        //    return RedirectToAction("Step1");
        //}
        public ActionResult Step1()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Step1")]
        public ActionResult PostStep1()
        {
            return RedirectToAction("Step2", new { area = "UI" });
        }

        public ActionResult Step2()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Step2")]
        public ActionResult PostStep2()
        {
            return RedirectToAction("Step3", new { area = "UI" });
        }

        public ActionResult Step3()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Step3")]
        public ActionResult PostStep3()
        {
            return RedirectToAction("Step4", new { area = "UI" });
        }
        public ActionResult Step4()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Step4")]
        public ActionResult PostStep4()
        {
            return RedirectToAction("Step5", new { area = "UI" });
        }

        public ActionResult Step5()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Step5")]
        public ActionResult PostStep5()
        {
            return RedirectToAction("Step6", new { area = "UI" });
        }
        public ActionResult Step6()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Step6")]
        public ActionResult PostStep6()
        {
            return RedirectToAction("Sign");
        }

        public ActionResult Sign()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Sign")]
        public ActionResult PostSign()
        {
            return RedirectToAction("Step7");
        }

        public ActionResult Step7()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Step7")]
        public ActionResult PostStep7()
        {
            return RedirectToAction("Index", "Pay", new { area = "UI" });
        }

        public ActionResult StepFinal()
        {
            return View();
        }

        public ActionResult Congrat()
        {
            return View();
        }

        public ActionResult Last()
        {
            return View();
        }
    }
}
