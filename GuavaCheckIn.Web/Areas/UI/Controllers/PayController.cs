﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuavaCheckIn.Web.Areas.UI.Controllers
{
    public class PayController : Controller
    {
        //
        // GET: /Pay/Index

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Bitcoin()
        {
            return View();
        }

        public ActionResult CreditCard()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Post()
        {
            return RedirectToAction("Congrat", "CheckIn", new { area = "UI" });
        }

    }
}
