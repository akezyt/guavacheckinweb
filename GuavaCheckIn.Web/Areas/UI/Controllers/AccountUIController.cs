﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuavaCheckIn.Web.Areas.UI.Controllers
{
    public class AccountUIController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ActionName("Register")]
        public ActionResult PostRegister(string email)
        {
            Session["Email"] = email;
            return RedirectToAction("Instruction", "CheckIn", new { area = "UI" });
        }
    }
}
